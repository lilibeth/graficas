@extends('layouts.app')

@section('content')
<div class="container">
    <div class="login-form">
        <div class="main-div">
           <div class="panel">
              <h2>Registro</h2>
              <p>Por favor, introduzca su información</p>
           </div>
            <form id="Login" class="form-horizontal" method="POST" action="{{ route('register') }}">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">                    
                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus placeholder='Nombre'>

                    @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required placeholder="Correo electrónico">

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <input id="password" type="password" class="form-control" name="password" required placeholder="Contraseña">

                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                </div>

                <div class="form-group">                    
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="Confirmar contraseña">
                </div>
                           
                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>

            </form>
        </div>
    </div>
</div>
@endsection
