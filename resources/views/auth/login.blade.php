@extends('layouts.app')

@section('content')
<div class="container">
    <div class="login-form">
        <div class="main-div">
           <div class="panel">
              <h2>Iniciar sesión</h2>
              <p>Por favor, introduzca su correo electrónico y contraseña</p>
           </div>
            <form id="Login" class="form-horizontal" method="POST" action="{{ route('login') }}">
               
               {{ csrf_field() }}

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus placeholder='Correo electrónico'>

                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                   <input id="password" placeholder='Contraseña' type="password" class="form-control" name="password" required>

                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                </div>
               <div class="form-group">
                     <div class="checkbox">
                        <label>
                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Recordar sesión
                        </label>
                    </div>
                </div>
               <!--<div class="forgot">
                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                   Olvido su contraseña?
                                </a>
                </div>-->
               <button type="submit" class="btn btn-primary">
                                    Iniciar sesión
                                </button>

            </form>
        </div>
    </div>
</div>
@endsection
