@extends('layouts.admin1')

@section('script')

   <script type="text/javascript" src="js/highstock.all.js"></script>
    <script type="text/javascript" >
            function crearte_data(){
                var data = [];
                var series = ['Gastos', 'Compras', 'Ventas'];
                $.each( series, function( index, name ){
                    var result = [];
                    var list_date = [];
                    for (var i=0; i<20; i++) {
                        var dia = numero_aleatorio(01, 30);
                        var mes = numero_aleatorio(01, 12);
                        var año = numero_aleatorio(2000, 2018);
                        var date = año+"-"+mes+"-"+dia+" 00:00:00";;
                        date = date.split(" - ").map(function (date){
                            return Date.parse(date+"-0500")/1000;
                        });
                        date = date+"000";
                        
                        list_date.push(parseInt(date));
                    }
                    list_date = list_date.sort(function de_menor_a_mayor(elem1, elem2) {
                return elem1-elem2;
            });
                    $.each( list_date, function( index, date ){
                        var value = numero_aleatorio(5000, 500000);
                        result.push([date, value]);
                    });
                    data.push(
                        {
                            'name': name,
                            'id': 'primary',
                            'dataGrouping': {
                                'forced': 'true',
                                'approximation': 'sum',
                                'smoothed': 'true',
                                'units': [
                                    ['day', [1]]
                                ]
                            },
                            'downsample': {
                                 'threshold': 1000
                            },
                            'data': result
                        }
                    );
                });
                
                return data
            }
            function numero_aleatorio(min, max) {
              return Math.round(Math.random() * (max - min) + min);
            }
            
            function update_series($chart_container, unit){
                $.each($chart_container.series, function(index, serie){
                    serie.update({
                        dataGrouping: {
                            forced: true,
                            approximation: "sum",
                            smoothed: true,
                            units: [
                                [unit, [1]]
                            ]
                        }
                    }, false);
                });
                $chart_container.redraw();
                return false
            }
                $(document).ready(function (){
                    var data = crearte_data();
                    console.log(data);
                     Highcharts.setOptions({
                        lang: {
                            loading: "Cargando...",
                            months: "['Enero', 'Febrero', 'Marzo', 'Abril','Mayo' 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']",
                            shortMonths: "['Ene', 'Feb', 'Mar', 'Ab', 'May', 'Jun', 'Jul', 'Agos', 'Sep', 'Oct', 'Nov', 'Dic']",
                            weekdays: "['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado']",
                        },
                    });
                    var char = Highcharts.stockChart('chart', {
                        rangeSelector: {
                            selected: 5,
                            enabled: true
                        },
                        title: 'Datos aleatorios ultimo años',
                        subtitle: 'Bráfica de tiempo',
                        series: data,
                        legend: {
                            enabled: true,
                        },
                    });
                    var name_container = "chart";
                    var $container = $('#'+name_container);
                    var $chart_container = $container.highcharts();        
                    

                    $("#group_monthly"+name_container).click(function () {
                        update_series($chart_container, 'month');
                        return false
                    });


                    $("#group_annually"+name_container).click(function () {
                        update_series($chart_container, 'year');
                        return false
                    });


                    $("#group_daily"+name_container).click(function () {
                        update_series($chart_container, 'day');
                        return false
                    });

                   
                });

</script>

@endsection
@section('active_graficar')
   class="active"
@endsection

@section('content')

   <div id="chart" ></div>   

@endsection